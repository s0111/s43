let posts = [ ];
let count = 1;


//Add post data.

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    e.preventDefault(); //stop or preventing the user from processing the default action of the object selected.
    posts.push({
        id: count,
        title: document.querySelector('#txt-title').value,
        body: document.querySelector('#txt-body').value
    });

    count ++;
    console.log(posts);
    showPosts(posts)
    alert('Successfully added.')

    document.querySelector('#txt-title').value = null;
    document.querySelector('#txt-body').value = null;
});

// Show Posts
const showPosts = (posts) => {
    let postEntries = '';

    posts.forEach((post) => {
        postEntries += `
        <div id = "post-${post.id}">

            <h3 id="post-title-${post.id}">${post.title}</h3>

            <p id="post-body-${post.id}">${post.body}</p>

            <button onclick = "editPost('${post.id}')">Edit</button>
            <button onclick = "deletePost('${post.id}')">Delete</button>
        </div>
        `
    });

    document.querySelector('#div-post-entries').innerHTML = postEntries;
}

//edit post function
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}

//Update Posts

document.querySelector('#form-edit-post').addEventListener('submit' ,(e) => {
    e.preventDefault();

    for (let i = 0; i < posts.length; i++) {
        console.log(posts[i].id)//Number
        if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;

            showPosts(posts);
            alert("Successfully update");
            break;
        }
        
    }

})

//Delete

const deletePost = (id) => {
    posts = posts.filter((post) => {
        if(post.id.toString() !== id){
            return post;
        }
    })
        document.querySelector(`#post-${id}`).remove()
}   


fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
// .then(data => console.log(data))
// .then(data => showPosts(data))
.then(data => {
    if(data == null) {
        alert("no data")
    } else {
        alert('successfully posted data')
    }
})